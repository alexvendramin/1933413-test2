
public class SalariedEmployee implements Employee{
	double yearlySalary;
	/**
	 * 
	 * @return the base yearly salary for an employee payed by the hour
	 */
	public double getWeeklyPay() {
		double newSalary = this.yearlySalary / 52;
		return newSalary;
	}
	/**
	 * 
	 * @return their yearly salary
	 */
	public double getYearlySalary() {
		return this.yearlySalary;
	}
	/**
	 * constructor method
	 * @param salary salary passed to constructor
	 */
	public SalariedEmployee(int salary) {
		this.yearlySalary = salary;
	}
}
