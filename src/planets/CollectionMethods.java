package planets;

import java.util.*;

public class CollectionMethods {
	/**
	 * takes a collection of planets and returns that collection with only 
	 * the planets larger than a certain size
	 * @param planets collection of planets
	 * @param size size that planets must be larger than
	 * @return planets that are larger than the size
	 */
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		ArrayList<Planet> array = new ArrayList<Planet>();
		for(Planet newPlanet : planets) {
			if(newPlanet.getRadius() >= size) {
				array.add(newPlanet);
			}
		}
		return array;
	
	}
}
