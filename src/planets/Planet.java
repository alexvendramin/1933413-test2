package planets;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet>{
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	/**
	 * overriding equals to give equality of planet's names and system names
	 * @param planet planet to take in and conduct equality on
	 * @return whether or not the planet is equal to the other
	 */
	@Override
	public boolean equals(Object planet) {
		if(this.planetarySystemName.equals(((Planet) planet).getPlanetarySystemName())){
			if(this.name.equals(((Planet) planet).getName())) {
				return true;
			}
			else {
				return false;
			}
		}
		else {
			return false;
		}
	}
	/**
	 * combines the name on the planet and its system to make it compatible
	 * @return the concatinated name
	 */
	@Override
	public int hashCode() {
		String combined = this.planetarySystemName + this.name;
		return combined.hashCode();
	
	}
	/**
	 * compares two planets to know how to sort them
	 * @param planet planet to be sorted
	 * @return numeric value signifying where it should be sorted
	 */
	@Override
	public int compareTo(Planet planet) {
		if(this.name.charAt(0) < planet.getName().charAt(0)) {
			return -1;
		}
		if(this.name.charAt(0) > planet.getName().charAt(0)) {
			return 1;
		}
		if(this.radius < planet.getRadius()) {
			return -1;
		}
		if(this.radius > planet.getRadius()) {
			return 1;
		}
		return 0;
	}
	
}