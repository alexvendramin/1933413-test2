
public class UnionizedHourlyEmployee extends HourlyEmployee{
	int maxHoursPerWeek;
	double overtimeRate;
	/**
	 * calculations for a unionized employee's salary
	 * @return their pay once the calculations are complete
	 */
	public double getWeeklyPay() {
		double pay = 0;
		for(int i = 1; i <= hoursWorked; i++) {
			if(i <= maxHoursPerWeek) {
				pay += hourlyPay;
			}
			else {
				pay += hourlyPay * overtimeRate;
			}
		}
		return pay;
	}
	/**
	 * constructor for a unionized worked
	 * @param hoursWorked hours they worked total
	 * @param hourlyPay pay they get hourly
	 * @param maxHoursPerWeek hours they work until overtime kicks in
	 * @param overtimeRate rate of overtime to be added to their pay
	 */
	public UnionizedHourlyEmployee(int hoursWorked, double hourlyPay, int maxHoursPerWeek, double overtimeRate) {
		super(hoursWorked, hourlyPay);
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = overtimeRate;
	}
}
