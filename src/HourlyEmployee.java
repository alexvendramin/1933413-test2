
public class HourlyEmployee implements Employee{
	int hoursWorked;
	double hourlyPay;
	
	/**
	 * basic method to calculate an employee's weekly pay
	 * @return the weekly payment for an employee
	 */
	public double getWeeklyPay() {
		double pay = this.hoursWorked * this.hourlyPay;
		return pay;
	}
	/**
	 * 
	 * @return the hours an employee worked in a week
	 */
	public int getHoursWorked() {
		return this.hoursWorked;
	}
	/**
	 * 
	 * @return the amount an employee gets payed per hour
	 */
	public double getHourlyPay() {
		return this.hourlyPay;
	}
	/**
	 * constructor method
	 * @param hoursWorked amount of hours an employee worked in a week
	 * @param hourlyPay the amount an employee gets payed per hour
	 */
	public HourlyEmployee(int hoursWorked, double hourlyPay) {
		this.hoursWorked = hoursWorked;
		this.hourlyPay = hourlyPay;
	}
}
