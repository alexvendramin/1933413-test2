import java.util.*;

/**
 * base interface for any employee
 */
public interface Employee {
	public double getWeeklyPay();
}
