
public class PayrollManagement {
	/**
	 * initializes hard coded employees and prints the value of the sum of their pay
	 * @param args
	 */
	public static void main(String[] args) {
		Employee[] employees = new Employee[5];
		employees[0] = new SalariedEmployee(26000);
		employees[1] = new SalariedEmployee(52000);
		employees[2] = new HourlyEmployee(50, 90.0);
		employees[3] = new HourlyEmployee(56, 80.0);
		employees[4] = new UnionizedHourlyEmployee(56, 90.0, 50, 2);
		double totalExpenses = getTotalExpenses(employees);
		System.out.println(totalExpenses);
	}
	/**
	 * finds the value of the sum of their payments
	 * @param array the array filed with employees
	 * @return
	 */
	public static double getTotalExpenses(Employee[] array) {
		double expenses = 0;
		for(int i = 0; i < array.length; i++) {
			expenses += array[i].getWeeklyPay();
		}
		return expenses;
	}
}
